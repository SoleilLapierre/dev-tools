# How to use these linter configs

Notes: 
* The mypy and pycodestyle config files can be combined into one.
* mypy, mycodestyle and pylint will  all pick up these config files automatically if
  you copy them to the root directory of a repository and run the commands from there
  or a subrirectory.
* If using the Python linters automatically within VS Code, you may have to override the
  Python interpreter in your project-local VS Code settings to get it to use the right
  versions of the linters.

## Mypy

* Linux: `python3 -m mypy --config-file <path to setup.cfg> <path to code to be checked>`
* Windows: `py -3 -m mypy --config-file <path to setup.cfg> <path to code to be checked>`

## Pycodestyle

* Linux: `python3 -m pycodestyle --config=<path to setup.cfg> <path to code to be checked>`
* Windows: `py -3 -m pycodestyle --config=<path to setup.cfg> <path to code to be checked>`

## Pylint

* Linux: `python3 -m pylint --rcfile <path to setup.cfg> <path to code to be checked>`
* Windows: `py -3 -m pycodestyle --rcfile <path to setup.cfg> <path to code to be checked>`

## StyleCop:

Add the StyleCop.Analysers NuGet package to your project.

Then see the [instructions](https://github.com/DotNetAnalyzers/StyleCopAnalyzers/blob/master/documentation/EnableConfiguration.md)
from the json file.
