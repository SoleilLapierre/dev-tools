# How to use the IDE config files

# .vscode/settings.json

If you use VS Code in a Git repo and switch to the local tab in the program settings, any changes you make from the default
settings will be saved to .vscode/settings.json in the root directory of the repo. You should consider committing this file
to version control because then other developers also using VS Code in that repo will pick up, for examplem, the Python
linter settings for that repo.

# Visual Studio .vssettings

This is just a periodic dump of selected non-private settings from Visual Studio - mostly my syntax color highlighting
and code style preferences. To import, go to Tools->Import and Export Settings->Import selected environment settings and
walk through the wizard. You can use the same wizard to export your own. Be cautious about which ones you export as
some of them can potentially leak personal information.

# Visual Studio snippets

In Visual Studio, go to Tools->Code Snippets Manager, select CSharp and My Code Snippets, then click Import and select these files.
Note these snippets are specific to me because they references names that exist in some of my own projects.

# .editorconfig

Just copy this file into the root of a repository and editors that support it will find it automatically next time you
start them there. Some editors may need to be configured or have a plugin installed to use this file.
