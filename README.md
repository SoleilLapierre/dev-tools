# Development Tools

This is just a place for me to centralize my style guides, development tool configurations, miscellaneous useful tools and recorded knowledge for software development.

# IP Freely

Feel free to copy and modify these files for your own purposes; no need for attribution or anything.
